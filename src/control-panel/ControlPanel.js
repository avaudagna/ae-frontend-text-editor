import React, { Component } from 'react';
import './ControlPanel.css';

class ControlPanel extends Component {

    isApplied(tag) {
        return this.props.appliedTags.includes(tag);
    }

    getButtonClass(tag) {
        return this.isApplied(tag) ? 'applied' : null;
    }

    render() {
        const { applyTag, indent, applyStyle } = this.props;

        return (
            <div id="control-panel">
                <div id="format-actions">
                    <button onClick={() => applyTag('b')} disabled={this.isApplied('b')} className={this.getButtonClass('b')} type="button"><b>B</b></button>
                    <button onClick={() => applyTag('i')} disabled={this.isApplied('i')} className={this.getButtonClass('i')} type="button"><i>I</i></button>
                    <button onClick={() => applyTag('u')} disabled={this.isApplied('u')} className={this.getButtonClass('u')} type="button"><u>U</u></button>
                    <button onClick={() => indent()} className={this.getButtonClass('indent')} type="button">Indent</button>
                    <select onClick={(e) => applyStyle('color', e.target.value)} className='color-select'>
                        <option value="" selected="selected" hidden="hidden">Color</option>
                        <option value="red">Red</option>
                        <option value="blue">Blue</option>
                        <option value="green">Green</option>
                        <option value="black">Black</option>
                    </select>
                </div>
            </div>
        );
    }
}

export default ControlPanel;
