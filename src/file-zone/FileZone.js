import React, { Component } from 'react';
import './FileZone.css';
import ContentEditable from 'react-contenteditable'

export default class FileZone extends Component {
  constructor(props) {
    super(props)
    this.contentEditable = React.createRef();
  };

  render = () => {
    const { fileRef, fileHtml, onFileChange, onFileTextSelection} = this.props;

    return <div id="file-zone">
            <ContentEditable 
                id="file"
                innerRef={fileRef}
                html={fileHtml}
                disabled={false}
                onDoubleClick={e => onFileTextSelection(e)}
                onChange={e => onFileChange(e.target.value)}
            />
        </div>
  };
};