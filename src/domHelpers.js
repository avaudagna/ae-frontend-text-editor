/**
 * Traverses the node childs until it reaches the last one.
 * @param  {Element} node The initial node
 * @return {Element}      The last child
 */
export const getInnermostNode = (node) => {
    while(!!node.firstChild) {
        node = node.firstChild;
    }
    return node;
}

/**
 * Returns all tags between the node and the element with limitId.
 * @param  {Element} node    The initial node
 * @param  {String}  limitId The limitId
 * @return {String[]}        The found tags
 */
export const getParentTags = (node, limitId) => {
    const tags = [];
    while(node.id !== limitId) {
        if(!!node.tagName) {
            tags.push(node.tagName.toLowerCase());
        }
        node = node.parentElement;
    }
    return tags;
}


/**
 * Traverses the node parents until it finds an Element with parentTag or reaches one with limitId.
 * @param  {Element}      node       The initial node
 * @param  {String}       parentTag  The target tag
 * @param  {String}       limitId    The limitId
 * @return {Element|null}            The found Element or null
 */
export const getParentByTag = (node, parentTag, limitId) => {
    while(node.id !== limitId) {
        if(node.tagName && node.tagName === parentTag) {
            break;
        }
        node = node.parentElement;
    }
    return node.id === limitId ? null : node;
}


