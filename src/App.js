import React, {Component} from 'react';
import './App.css';
import ControlPanel from "./control-panel/ControlPanel";
import FileZone from "./file-zone/FileZone";
import SynonymList from './synonyms-list/SynonymList';
import getSynonyms from './text.service';
import {getInnermostNode, getParentTags, getParentByTag} from './domHelpers';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fileHtml: '',
            selectedRange: null,
            appliedTags: [],
            synonyms: [],
            loadingSynonyms: false
        }
        this.fileRef = React.createRef();
    }

    applyTag = (tag, style={}) => {
        //Surround selection with requested tag
        const rangeSelection = getSelection().getRangeAt(0)
        const newParent = document.createElement(tag);
        rangeSelection.surroundContents(newParent);

        //apply styles if any
        for (let [key, value] of Object.entries(style)) {
            newParent.style.setProperty(key, value);
        }

        this.setState({
            fileHtml: this.fileRef.current.innerHTML, 
            selectedRange: rangeSelection,
            appliedTags: getParentTags(getInnermostNode(newParent), 'file') //process from innermost element
        }, () => console.log(this.state));
    }


    onFileTextSelection = (e) => {
        const currentRange = getSelection().getRangeAt(0);
        getSynonyms(currentRange.toString()).then(
            (results) => this.setState({synonyms: results, loadingSynonyms: false}, 
                () => console.log(this.state)));
        this.setState({
            selectedRange: currentRange,
            appliedTags: getParentTags(currentRange.commonAncestorContainer, 'file'),
            loadingSynonyms: true 
        }, () => console.log(this.state));
    }

    replaceTextWithSynonym = (synonym) => {
        const { selectedRange } = this.state;
        selectedRange.extractContents();
        selectedRange.insertNode(document.createTextNode(synonym));
        this.setState({fileHtml: this.fileRef.current.innerHTML});
    }

    indent = () => {
        const { selectedRange } = this.state;
        //Search for styles span
        const stylesSpan = getParentByTag(selectedRange.commonAncestorContainer, 'SPAN', 'file');
        if(!!stylesSpan) {
            //if any, then increase margin-left
            stylesSpan.style.setProperty('margin-left', 
            parseInt(stylesSpan.style.getPropertyValue('margin-left').split('px')[0], 10)+20+"px");
            this.setState({fileHtml: this.fileRef.current.innerHTML},() => console.log(this.state));
        } else {
            //else create new span with indent margin-left around current selection
            this.applyTag('span', {'margin-left': '20px'});
        }
        selectedRange.collapse();
    }

    applyStyle = (name, value) => {
        const { selectedRange } = this.state;
        //Search for styles span
        const stylesSpan = getParentByTag(selectedRange.commonAncestorContainer, 'SPAN', 'file');
        if(!!stylesSpan) {
            //if any, then apply style
            stylesSpan.style.setProperty(name, value);
            this.setState({fileHtml: this.fileRef.current.innerHTML},() => console.log(this.state));
        } else {
            //else insert new span with applied style around current selection
            this.applyTag('span', {[name]: value});
        }
        selectedRange.collapse();
    }

    render() {
        const { fileHtml, appliedTags, synonyms, loadingSynonyms} = this.state;
        return (
            <div className="App">
                <header>
                    <span>Simple Text Editor</span>
                </header>
                <main>
                    <ControlPanel appliedTags={appliedTags} applyTag={this.applyTag} indent={this.indent} applyStyle={this.applyStyle} />
                    <div className="text-section">
                        <FileZone 
                            fileRef={this.fileRef}
                            fileHtml={fileHtml} 
                            onFileChange={(newHTML) => this.setState({fileHtml: newHTML}, () => console.log(this.state))}
                            onFileTextSelection={this.onFileTextSelection}/>
                        <SynonymList synonyms={synonyms} loading={loadingSynonyms} replaceText={this.replaceTextWithSynonym}/>
                    </div>
                </main>
            </div>
        );
    }
}

export default App;
