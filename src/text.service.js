export default function getSynonyms(word) {
    const url = new URL("https://api.datamuse.com/words/");
    const params = {ml: word}
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
    return fetch(url).then((res) => res.json()).then((synonyms) => synonyms.slice(0, 10).map(s => s.word));
}