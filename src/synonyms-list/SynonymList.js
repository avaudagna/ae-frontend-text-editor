import React from 'react';
import './SynonymList.css';

const SynonymList = ({ synonyms, loading, replaceText}) =>  {
    if (loading) {
        synonyms = ['Loading Synonyms...'];
    }
    return <div id="synonym-list-container">
        <h4 id="synonym-list-title">Word synonyms</h4>
        <span>Two clicks to replace word with selection</span>
        <ul id="synonym-list">
            {synonyms.map((synonym, i) => <li className="synonym" key={`syn-${i}`} onDoubleClick={()=>replaceText(synonym)}>{synonym}</li>)}
        </ul>
    </div>
}

export default SynonymList;